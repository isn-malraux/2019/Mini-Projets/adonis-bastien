#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 21 10:15:03 2018

@author: utilisateur
"""

from PIL import Image
cheminimage=input("Nom du fichier : ")
image=Image.open(cheminimage)
largeur=image.width
hauteur=image.height
numero_colonne=0
numero_ligne=0
for numero_ligne in range (0,hauteur):
    for numero_colonne in range (0,largeur):
        data=image.getpixel((numero_colonne,numero_ligne))
        data2=(data[2],data[1],data[0]) 
        """décalage RGB-BGR"""
        image.putpixel((numero_colonne,numero_ligne),(data2))
image.show()
        
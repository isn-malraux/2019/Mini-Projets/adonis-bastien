1 Menu principal
	1.1 Symetrie
		1.1.1 revenir au menu précédant
		1.1.2 symétrie axiale horizontale
		1.1.3 symetrie axiale verticale
	1.2 Afficher l'image
		1.2.1 donner l'adresse de l'image
	1.3 Rotation physique de l''image
		1.3.1 Revenir au menu précédant
		1.3.2 Retourner
		1.3.3 Droite
		1.3.4 Gauche
	1.4 Afficher les informations sur l'image
		1.4.1 Mode colorimétrique
		1.4.2 Dimension
		1.4.3 Format de l'image
		1.4.4 Fonction pour revenir au menu précédant 
	1.5 Commande de fonction
	1.6 Quitter le programme
	1.7 Ouvrir une image et l'afficher
	1.8 rotation des couleurs de l'image
		1.8.1 Décalage RGB=>GBR
		1.8.2 Décalage RGB=>BRG
		1.8.3 Décalage RGB=>BGR
		1.8.4 Fonction pour revenir au menu précédant
	1.9 Mettre l'image en negatif
